function required(name) {
    return {
        validator: (value) => value != "" && value !== undefined && value !== null,
        message: `Wymagana wartość dla: ${name}`
    }
}

function minLength(name, minlength) {
    return {
        validator: (value) => String(value).length >= minlength,
        message: `Przynajmniej ${minlength} znaków jest wymagane dla ${name}`
    }
}

function alpha(name) {
    return {
        validator: (value) => /^[a-zA-Z]*$/.test(value),
        message: `${name} może zawierać tylko litery`
    }
}

function numeric(name) {
    return {
        validator: (value) => /^[0-9]*$/.test(value),
        message: `${name} może zawierać tylko cyfry`
    }
}

function range(name, min, max) {
    return {
        validator: (value) => value >= min && value <= max,
        message: `${name} musi zawierać się pomiędzy ${min} a ${max}`
    }
}

export default {
    name: [minLength("Nazwa", 3)],
    category: [required("Kategorii"), alpha("Kategorii")],
    price: [numeric("Cena"), range("Cena", 1, 1000)]
}