# README #

Materiały szkoleniowe - Vue.js praktyka poparta teorią.

W każdym folderze znajduje się projekt zgodnie z oznaczeniem na ikonie "puzla" w prezentacji.

### Jak uruchomić ten kod? ###

```console
cd nazwa_folderu
npm install

npm run serve
```

W przypadku plików zawierających w pliku package.json dodany fragment

```javascript
"json": "json-server plik.js -p 3500"
```

W nowym oknie terminala / konsoli uruchamiamy polecenie

```console
npm run json
```