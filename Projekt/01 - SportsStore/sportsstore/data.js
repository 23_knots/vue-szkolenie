var data = [{ id: 1, name: "Kajak", category: "Sporty wodne", 
                description: "Łódka dla jednej osoby", price: 275 },
            { id: 2, name: "Kamizelka ratunkowa", category: "Sporty wodne", 
                description: "Ochronna i modna", price: 48.95 },
            { id: 3, name: "Piłka do piłki nożnej", category: "Piłka nożna", 
                description: "Rozmiar i waga zgodna z przepisami FIFA", price: 19.50 },
            { id: 4, name: "Flagi", category: "Piłka nożna", 
                description: "Profesjonalizm na Twoim boisku", 
                price: 34.95 },
            { id: 5, name: "Stadion", category: "Piłka nożna", 
                description: "Ładnie zapakowany 35 tysięczny stadion", price: 79500 },
            { id: 6, name: "Mądra czapka", category: "Szachy", 
                description: "Mądrość +75", price: 16 },
            { id: 7, name: "Niewygodne krzesło", category: "Szachy", 
                description: "Dyskomfort dla Twojego przeciwnika", 
                price: 29.95 },
            { id: 8, name: "Szachy", category: "Szachy", 
                description: "Rozrywka dla całej rodziny", price: 75 },
            { id: 9, name: "Bling Bling King", category: "Szachy", 
                description: "Pozłacana figura króla", price: 1200 }]

module.exports = function () {
    return { 
        products: data,
        categories: [...new Set(data.map(p => p.category))].sort(),
        orders: []
    }
}
